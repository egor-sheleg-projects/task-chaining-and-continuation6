﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace TaskChainingAndContinuation.Test
{
    /// <summary>
    /// Tests for array controller.
    /// </summary>
    [TestClass]
    public class ArrayTests
    {
        /// <summary>
        /// Testing multipling.
        /// </summary>
        [TestMethod]
        public void MultiplyArrayThrowingNullExeption()
        {
            // arrange
            int[] array = this.SetUp();

            // assert     
            Assert.ThrowsException<ArgumentNullException>(() =>
                Controller.MultiplyArray(null));
        }

        /// <summary>
        /// Testing sorting.
        /// </summary>
        [TestMethod]
        public void SortArrayThrowingNullExeption()
        {
            // arrange
            int[] array = this.SetUp();

            // assert     
            Assert.ThrowsException<ArgumentNullException>(() =>
                Controller.SortArray(null));
        }

        /// <summary>
        /// Testing average.
        /// </summary>
        [TestMethod]
        public void AverageArrayThrowingNullExeption()
        {
            // arrange
            int[] array = this.SetUp();

            // assert     
            Assert.ThrowsException<ArgumentNullException>(() =>
                Controller.AverageArray(null));
        }

        /// <summary>
        /// Creating a database element.
        /// </summary>
        [SetUp]
        public int[] SetUp()
        {
            return Controller.CreateArray();
        }
    }
}
