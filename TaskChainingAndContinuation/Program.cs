﻿using System;
using System.Threading.Tasks;

namespace TaskChainingAndContinuation
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Task<int[]> Task1 = new Task<int[]>(() => Controller.CreateArray());

            Task<int[]> Task2 = Task1.ContinueWith(task => Controller.MultiplyArray(task.Result));

            Task<int[]> Task3 = Task2.ContinueWith(task => Controller.SortArray(task.Result));

            Task<int> Task4 = Task3.ContinueWith(task => Controller.AverageArray(task.Result));

            Task1.Start();

            Task4.Wait();
            Console.WriteLine("End of Main");
        }
    }
}
