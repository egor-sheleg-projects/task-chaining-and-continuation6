﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskChainingAndContinuation
{
    public class Controller
    {
        public static int[] CreateArray()
        {
            int[] array = new int[10];
            Random randNum = new Random();
            Console.WriteLine("Created array:");
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = randNum.Next();
                Console.Write(array[i] + " ");
            }
            Console.WriteLine("");

            return array;
        }

        public static int[] MultiplyArray(int[] source)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            int[] result = new int[source.Length];
            int randNum = new Random().Next(-10, 10);
            Console.WriteLine("Random Number: " + randNum);
            Console.WriteLine("Modificated array:");
            for (int i = 0; i < source.Length; i++)
            {
                result[i] = source[i] * randNum;
                Console.Write(result[i] + " ");
            }
            Console.WriteLine("");

            return result;
        }

        public static int[] SortArray(int[] source)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            Array.Sort(source);
            Console.WriteLine("Sorted array:");
            foreach (int i in source)
            {
                Console.Write(i + " ");
            }
            Console.WriteLine("");
            return source;
        }

        public static int AverageArray(int[] source)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            int result = 0;
            Console.WriteLine("Averrage of array:");
            for (int i = 0; i < source.Length; i++)
            {
                result += source[i];
            }

            Console.WriteLine(result / source.Length);

            return result / source.Length;
        }
    }
}
